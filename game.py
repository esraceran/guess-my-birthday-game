from random import randint



name = input("Hi! What is your name?")

for i in range(1,6):

    guess_month = randint(1,12)
    guess_year= randint(1924, 2004)


    print("Guess number ", i,":", name , "were you born in", guess_month, "/", guess_year)

    user_answer = input("yes/no?")


    if user_answer == "yes":
        print("I knew it!")
        break
    elif user_answer == "no":
        if i < 5:
            print("Drat! Lemme try again!")
        else:
            print("I have other things to do. Good bye")
    else:
        print("Please answer yes or no")
